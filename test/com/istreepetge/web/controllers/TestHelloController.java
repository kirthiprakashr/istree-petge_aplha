package com.istreepetge.web.controllers;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.springframework.web.servlet.ModelAndView;

public class TestHelloController {

	@Test
	public void test() throws Exception {
		HelloController controller = new HelloController();
		ModelAndView modelAndView = controller.handleRequest(null);
		assertEquals("home.jsp", modelAndView.getViewName());
	}

}
