package com.istreepetge.web.controllers;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.istreepetge.models.IpCalendarEvent;

@Controller
@RequestMapping(value = "/handle.html")
public class HelloController {

	protected final Log logger = LogFactory.getLog(getClass());

	@RequestMapping(method = RequestMethod.POST, params = "bean=IpCalendarEvent")
	public ModelAndView handleRequest(
			@ModelAttribute("IpCalendarEvent") IpCalendarEvent ice)
			throws Exception {
		logger.info("IpCalendarEvent: " + ice);
		ModelAndView mv = new ModelAndView();
		mv.setViewName("hello");
		mv.addObject("msg", "SUCCESS - " + ice.getId());
		mv.addObject("IpCalendarEvent", ice);
		return mv;
	}
}
