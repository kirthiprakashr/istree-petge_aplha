package com.istreepetge.web.controllers;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.google.api.client.googleapis.auth.oauth2.draft10.GoogleAuthorizationRequestUrl;

@Controller
@RequestMapping(value = "/login.html")
public class Login {

	@Value("#{apiproperties['clientId']}")
	private String clientId;

	@Value("#{apiproperties['redirectUrl']}")
	private String redirectUrl;

	@Value("#{apiproperties['scope']}")
	private String scope;

	protected final Log logger = LogFactory.getLog(getClass());

	@RequestMapping(method = RequestMethod.GET)
	public void redirectToGoogle(HttpServletResponse response) {
		logger.info(redirectUrl + " - " + scope + " - " + clientId);
		String authorizationUrl = new GoogleAuthorizationRequestUrl(clientId,
				redirectUrl, scope).build();
		try {
			response.sendRedirect(authorizationUrl);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
