package com.istreepetge.models;

import java.util.Random;

public class IpCalendarEvent {
	private Long id;
	private String event;
	private Double spentAmount;
	private String tags;

	public IpCalendarEvent() {
		Random r = new Random();
		setId(r.nextLong());
	}

	public Long getId() {
		return id;
	}

	private void setId(Long id) {
		this.id = id;
	}

	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	public Double getSpentAmount() {
		return spentAmount;
	}

	public void setSpentAmount(Double spentAmount) {
		this.spentAmount = spentAmount;
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	@Override
	public String toString() {
		return "IpCalendarEvent [id=" + id + ", event=" + event
				+ ", spentAmount=" + spentAmount + ", tags=" + tags + "]";
	}

}
