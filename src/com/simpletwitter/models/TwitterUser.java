package com.simpletwitter.models;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "twitter_user")
public class TwitterUser {

	private Long objId;
	private String username;
	private String password;
	private String emailId;
	private Set<TwitterUser> followers = new HashSet<TwitterUser>();
	private Set<TwitterUser> following = new HashSet<TwitterUser>();
	private Date dateCreated;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getObjId() {
		return objId;
	}

	public void setObjId(Long objId) {
		this.objId = objId;
	}

	@Column(name = "username")
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Column(name = "password", length = 1024)
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Column(name = "email_id")
	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "follow", joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "objId"), inverseJoinColumns = @JoinColumn(name = "following_id", referencedColumnName = "objId"))
	public Set<TwitterUser> getFollowers() {
		return followers;
	}

	public void setFollowers(Set<TwitterUser> followers) {
		this.followers = followers;
	}

	@ManyToMany(mappedBy = "followers", fetch = FetchType.EAGER)
	public Set<TwitterUser> getFollowing() {
		return following;
	}

	public void setFollowing(Set<TwitterUser> following) {
		this.following = following;
	}

	@Column(name = "date_created")
	public Date getDateCreated() {
		return this.dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	@Override
	public String toString() {
		return "TwitterUser [objId=" + objId + ", username=" + username
				+ ", emailId=" + emailId + ", dateCreated=" + dateCreated + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((emailId == null) ? 0 : emailId.hashCode());
		result = prime * result + ((objId == null) ? 0 : objId.hashCode());
		result = prime * result
				+ ((password == null) ? 0 : password.hashCode());
		result = prime * result
				+ ((username == null) ? 0 : username.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof TwitterUser)) {
			return false;
		}
		TwitterUser other = (TwitterUser) obj;
		if (emailId == null) {
			if (other.emailId != null) {
				return false;
			}
		} else if (!emailId.equals(other.emailId)) {
			return false;
		}
		if (objId == null) {
			if (other.objId != null) {
				return false;
			}
		} else if (!objId.equals(other.objId)) {
			return false;
		}
		if (password == null) {
			if (other.password != null) {
				return false;
			}
		} else if (!password.equals(other.password)) {
			return false;
		}
		if (username == null) {
			if (other.username != null) {
				return false;
			}
		} else if (!username.equals(other.username)) {
			return false;
		}
		return true;
	}

}
