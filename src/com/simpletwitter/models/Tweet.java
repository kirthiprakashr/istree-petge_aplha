package com.simpletwitter.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tweet")
public class Tweet {

	private Long objId;
	private String tweetMessage;
	private TwitterUser twitterUser;
	private Date dateCreated;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getObjId() {
		return objId;
	}

	public void setObjId(Long objId) {
		this.objId = objId;
	}

	@Column(name = "tweet_message", length = 1024)
	public String getTweetMessage() {
		return tweetMessage;
	}

	public void setTweetMessage(String tweetMessage) {
		this.tweetMessage = tweetMessage;
	}

	@ManyToOne()
	@JoinColumn(name = "twitter_user_obj_id", nullable = false)
	public TwitterUser getTwitterUser() {
		return twitterUser;
	}

	public void setTwitterUser(TwitterUser twitterUser) {
		this.twitterUser = twitterUser;
	}

	@Column(name = "date_created")
	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((dateCreated == null) ? 0 : dateCreated.hashCode());
		result = prime * result + ((objId == null) ? 0 : objId.hashCode());
		result = prime * result
				+ ((tweetMessage == null) ? 0 : tweetMessage.hashCode());
		result = prime * result
				+ ((twitterUser == null) ? 0 : twitterUser.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Tweet)) {
			return false;
		}
		Tweet other = (Tweet) obj;
		if (dateCreated == null) {
			if (other.dateCreated != null) {
				return false;
			}
		} else if (!dateCreated.equals(other.dateCreated)) {
			return false;
		}
		if (objId == null) {
			if (other.objId != null) {
				return false;
			}
		} else if (!objId.equals(other.objId)) {
			return false;
		}
		if (tweetMessage == null) {
			if (other.tweetMessage != null) {
				return false;
			}
		} else if (!tweetMessage.equals(other.tweetMessage)) {
			return false;
		}
		if (twitterUser == null) {
			if (other.twitterUser != null) {
				return false;
			}
		} else if (!twitterUser.equals(other.twitterUser)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Tweet [objId=" + objId + ", tweetMessage=" + tweetMessage
				+ ", twitterUser=" + twitterUser + ", dateCreated="
				+ dateCreated + "]";
	}

}
