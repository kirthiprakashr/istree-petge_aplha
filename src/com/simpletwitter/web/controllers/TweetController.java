package com.simpletwitter.web.controllers;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.simpletwitter.common.Authenticator;
import com.simpletwitter.models.Tweet;
import com.simpletwitter.models.TwitterUser;
import com.simpletwitter.service.TweetService;
import com.simpletwitter.service.TwitterUserService;

@Controller
public class TweetController {

	@RequestMapping(value = "/tweet.html", method = RequestMethod.POST, params = "bean=tweet")
	public ModelAndView handleRequest(@ModelAttribute("tweet") Tweet tweet,
			HttpServletRequest request) throws Exception {
		TwitterUser currUser = Authenticator.isValid(request);
		ModelAndView mv = new ModelAndView();
		if (currUser != null) {
			if (tweet.getTweetMessage() != null
					&& !"".equals(tweet.getTweetMessage())) {
				if (tweet.getTweetMessage().length() <= 100) {
					tweet.setDateCreated(new Date());
					tweet.setTwitterUser(currUser);
					TweetService.createTweet(tweet);
				} else {
					mv.addObject("tweetlengtherror",
							"Not more than 100 charcters are allowed");
				}
			}
			Set<Tweet> tweets = TweetService.getTweetStream(
					currUser.getObjId(), currUser.getFollowing());
			Set<TwitterUser> allUserList = TwitterUserService
					.getAllUserList(currUser);
			Map<String, Integer> followCount = TwitterUserService
					.getFollowCount(currUser.getObjId());
			int tweetCount = TwitterUserService.getTweetCount(currUser);
			Tweet eTweet = new Tweet();
			mv.addObject("tweet", eTweet);
			mv.setViewName("bstwitterhomepage");
			mv.addObject("tweets", tweets);
			mv.addObject("user", currUser);
			mv.addObject("userlist", allUserList);
			mv.addObject("followcount", followCount);
			mv.addObject("tweetcount", tweetCount);
		} else {
			Authenticator.loginFailed(mv, null);
		}
		return mv;
	}

	@RequestMapping(value = "/tweetdelete.html", method = RequestMethod.GET, params = "objid")
	public ModelAndView viewUser(@RequestParam(value = "objid") String objId,
			HttpServletRequest request) {
		TwitterUser currUser = Authenticator.isValid(request);
		ModelAndView mv = new ModelAndView();
		if (currUser != null) {
			Tweet tweet = TweetService.getTweet(Long.valueOf(objId));
			if (currUser.equals(tweet.getTwitterUser())) {
				TweetService.deleteTweet(tweet);
			}
			Set<Tweet> tweets = TweetService.getTweetStream(
					currUser.getObjId(), currUser.getFollowing());
			Set<TwitterUser> allUserList = TwitterUserService
					.getAllUserList(currUser);
			Map<String, Integer> followCount = TwitterUserService
					.getFollowCount(currUser.getObjId());
			int tweetCount = TwitterUserService.getTweetCount(currUser);
			Tweet eTweet = new Tweet();
			mv.addObject("tweet", eTweet);
			mv.setViewName("bstwitterhomepage");
			mv.addObject("tweets", tweets);
			mv.addObject("user", currUser);
			mv.addObject("userlist", allUserList);
			mv.addObject("followcount", followCount);
			mv.addObject("tweetcount", tweetCount);
		} else {
			Authenticator.loginFailed(mv, null);
		}
		return mv;
	}
}
