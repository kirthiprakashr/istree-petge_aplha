package com.simpletwitter.common;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.ModelAndView;

import com.simpletwitter.models.TwitterUser;

public class Authenticator {

	public static TwitterUser isValid(HttpServletRequest request) {
		HttpSession session = request.getSession();
		TwitterUser currUser = (TwitterUser) session
				.getAttribute("twitteruser");
		return currUser;
	}

	public static void loginFailed(ModelAndView mv, String errorMsg) {
		TwitterUser tuser = new TwitterUser();
		mv.addObject("TwitterUser", tuser);
		if (errorMsg != null) {
			mv.addObject("errormsg", errorMsg);
		}
		mv.setViewName("loginorregister");
	}

	public static void saveUserToSession(HttpServletRequest request,
			TwitterUser objectByExample) {
		HttpSession session = request.getSession();
		session.setAttribute("twitteruser", objectByExample);
	}
}
