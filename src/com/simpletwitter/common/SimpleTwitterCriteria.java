package com.simpletwitter.common;

import java.util.ArrayList;
import java.util.List;

public class SimpleTwitterCriteria {
	private List<SimpleTwitterCriterion> criterionList = new ArrayList<SimpleTwitterCriterion>();

	private SimpleTwitterCriteria() {
		// TODO Auto-generated constructor stub
	}

	public static SimpleTwitterCriteria instance() {
		return new SimpleTwitterCriteria();
	}

	public SimpleTwitterCriteria add(SimpleTwitterCriterion criterion) {
		criterionList.add(criterion);
		return this;
	}

	public List<SimpleTwitterCriterion> getCriterionList() {
		return criterionList;
	}

}
